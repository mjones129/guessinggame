# guessingGame

Android app that generates a random number, then invites users to guess what the number is. If statements let the user know if they need to go higher, lower, or they got it! How exciting!