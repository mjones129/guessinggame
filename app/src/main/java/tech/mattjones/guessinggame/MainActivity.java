package tech.mattjones.guessinggame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {


    int randomNumber;

    public void makeToast(String string) {
        Toast.makeText(MainActivity.this, string, Toast.LENGTH_SHORT).show();
    }


    public void giveHint (View view) {
        EditText userGuess = findViewById(R.id.userGuess);

        int guessInt = Integer.parseInt(userGuess.getText().toString());

        if (guessInt > randomNumber) {
            makeToast("Lower...");
        } else if (guessInt < randomNumber) {
            makeToast("Higher...");
        } else {
            makeToast("You guessed it! Go ahead, try again");
            Random rn = new Random();
            randomNumber = rn.nextInt(20) + 1;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Random rn = new Random();
        randomNumber = rn.nextInt(20) + 1;
    }
}
